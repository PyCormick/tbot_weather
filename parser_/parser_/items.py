# Define here the models for your scraped items
#
# See documentation in:
# https://docs.scrapy.org/en/latest/topics/items.html

import scrapy


class ParserItem(scrapy.Item):
    date = scrapy.Field()
    precipitation = scrapy.Field()
    precipitation_count = scrapy.Field()
    temp = scrapy.Field()
    wind = scrapy.Field()
    pressure = scrapy.Field()
