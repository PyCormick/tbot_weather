import scrapy

from ..items import ParserItem


class TzSpiderSpider(scrapy.Spider):
    name = 'tz_spider'
    allowed_domains = ['pogoda33.ru']
    start_urls = [
        'https://pogoda33.ru/%D0%BF%D0%BE%D0%B3%D0%BE%D0%B4%D0%B0-'
        '%D1%8F%D1%80%D0%BE%D1%81%D0%BB%D0%B0%D0%B2%D0%BB%D1%8C/%D0%BC%D0%B5%D1%81%D1%8F%D1%86'
    ]

    def parse(self, response):
        day_list = response.xpath(
            "//div/div[@class='day col-md p-2 month-border border-left-0 border-top-0 text-truncate '"
            " or @class='day col-md p-2 month-border border-left-0 border-top-0 text-truncate  weekend']")
        return self.parse_list(day_list)

    def parse_list(self, list_):
        for item_ in list_:
            yield self.parse_days(item_)

    def parse_days(self, item):
        item_ = ParserItem()
        item_["precipitation"] = item.xpath("div/span[@class='precipitation-description']/text()").get()
        item_["precipitation_count"] = item.xpath("div/span[@class='precipitation-count']/text()").get()
        item_["temp"] = item.xpath("div/span[@class='forecast-temp temp']/text()").get().strip()
        item_["wind"] = item.xpath("div/div/span[@class='wind-count']/text()").get()
        item_["pressure"] = item.xpath("div/div/span[@class='pressure-count']/text()").get()
        item_["date"] = " ".join(item.xpath("h5[@class='row align-items-center']/span/text()").getall())

        return item_
