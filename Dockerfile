FROM python:3

RUN apt-get update
RUN apt-get install -y locales locales-all

RUN mkdir /tbot
WORKDIR /tbot
COPY . /tbot/

RUN pip install poetry && poetry config virtualenvs.create false && poetry install --no-dev

CMD ./run.sh
