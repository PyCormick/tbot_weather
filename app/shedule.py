import schedule
import time
import os


def job():
    if os.path.exists("../items.json"):
        os.remove("../items.json")

    os.system("cd parser_ && poetry run scrapy crawl tz_spider")


schedule.every().days.at("04:30").do(job)


while True:
    schedule.run_pending()
    print("Schedule is running...")
    time.sleep(100)
