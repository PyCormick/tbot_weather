import json


def get_weather_for_date(date: str):
    with open('../items.json', 'r') as f:
        items = json.load(f)
        for item in items:
            if date == item["date"]:
                prec = item["precipitation"].capitalize()
                temp = f'Температура {item["temp"]} C'

                prec_count = f'Кол-во осадков {item["precipitation_count"]}' \
                    if item["precipitation_count"] \
                    else "Без осадков"
                wind = f'Скорость ветра {item["wind"]}' \
                    if item["wind"] \
                    else ""
                pressure = f'Давление {item["pressure"]}' \
                    if item["pressure"] \
                    else ""

                out_data = f"{prec}\n" \
                           f"{prec_count}\n" \
                           f"{temp}\n" \
                           f"{wind}\n" \
                           f"{pressure}\n"

                return out_data
